import matplotlib
matplotlib.use('TkAgg')

import numpy as np
from agent import *
from prisoners import *
from copy import copy, deepcopy
from tqdm import tqdm

import matplotlib.pyplot as plt
from matplotlib import colors


class Stats:
	def __init__(self, n_races):
		self.n_agents = 0;
		self.n_races = n_races;
		self.alive_races = 0;
		
		self.race_counts = [0] * self.n_races;
		self.altruism = [0] * self.n_races;
		self.nationalism = [0] * self.n_races;	
		self.invasion = [0] * self.n_races;		
		self.genes = [np.array([0,0,0,0])] * self.n_races;
		
		

class Game:
	def __init__(self, n_races = 2, grid_size = 50, radius = 1):
		self.n_races = n_races;
		self.grid_size = grid_size;
		self.board = [[None]*self.grid_size for i in range(self.grid_size)];
		self.radius = radius;
		self.time = 0;
		
	def simulate(self):
		# random order
		ridxs0 = np.arange(self.grid_size);
		np.random.shuffle(ridxs0);
		ridxs1 = np.arange(self.grid_size);
		np.random.shuffle(ridxs1);
	
		# migrate
		positions = [];
		
		for i in ridxs0:
			for j in ridxs1:
				if self.board[i][j] is not None:
					positions.append((i,j));
		
		visited = set();
		for i in ridxs0:
			for j in ridxs1:
				if not self.free(i,j) and (not (i,j) in visited):
					if self.board[i][j].migrate():
						if len(positions) > 0:
							ri,rj = positions[np.random.choice(len(positions))];
							neighs = self.get_neighbors(ri,rj);
							for nei in neighs:
								if self.free(nei):
									ni,nj = nei;
									self.move(i,j,ni,nj);
									visited.add((ni,nj))
									break;
		
		# compete
		visited = set();
		for i in ridxs0:
			for j in ridxs1:
				if not self.free(i,j):
					neighs = self.get_neighbors(i,j);
					for nei in neighs:
						ni,nj = nei
						if not self.free(nei):
							if not (ni,nj) in visited:							
								compete(self.board[i][j], self.board[ni][nj]);
					visited.add((i,j))
		
		# birth
		visited = set();
		for i in ridxs0:
			for j in ridxs1:
				if not self.free(i,j) and (not (i,j) in visited):
					if self.board[i][j].reproduce():
						neighs = self.get_neighbors(i,j);
						for nei in neighs:
							ni,nj = nei
							if self.free(nei):
								self.board[ni][nj] = deepcopy(self.board[i][j]);
								self.board[ni][nj].mutate();
								visited.add((ni,nj));
					self.board[i][j].reset_reproduction();
		# death
		for i in ridxs0:
			for j in ridxs1:
				if not self.free(i,j):
					if self.board[i][j].die():
						self.board[i][j] = None;
						
		self.time += 1;
		
	def get_neighbors(self, ii,jj = None):
		if jj is None:
			i,j = ii;
		else:
			i = ii;
			j = jj;
		
		ret = [];
		
		for di in range(-self.radius,self.radius+1):
			for dj in range(-self.radius,self.radius+1):
				if di == 0 and dj == 0:
					continue;
				coord = ((i+di)%self.grid_size,(j+dj)%self.grid_size);
				ret.append(coord);
		
		# ret = [((i+1)%self.grid_size,j), ((i-1)%self.grid_size,j), (i,(j+1)%self.grid_size), (i,(j-1)%self.grid_size)];
		np.random.shuffle(ret);
		return ret;
	

	def free(self, ii,jj = None):
		if jj is None:
			i,j = ii;
		else:
			i = ii;
			j = jj;
			
		return self.board[i][j] is None;


	def move(self, i0, j0, i1, j1):
		self.board[i1][j1] = self.board[i0][j0];
		self.board[i0][j0] = None;


	def stats(self):
		s = Stats(self.n_races);		
		
		races = set();
		for i in range(self.grid_size):
			for j in range(self.grid_size):
				if not self.free(i,j):
					race = self.board[i][j].race;
					races.add(race);
					s.race_counts[race] += 1;
					# genes = np.array([int(k) for k in list(self.board[i][j].genetics)]);
					genes = np.array(self.board[i][j].genetics);
					
					s.genes[race] = s.genes[race] + genes;
					s.n_agents += 1;

					nat = 0;
					alt = 0;
					for k in range(len(genes)):
						if k == race:
							nat += genes[k];
						else:
							alt += genes[k];
					alt = alt / (len(genes)-1);
					
					s.nationalism[race] += nat;
					s.altruism[race] += alt;
					
					s.invasion[race] += self.board[i][j].p_migrate;
		
		for k in range(len(s.genes)):
			s.altruism[k]       /= np.max((s.race_counts[k],1));
			s.nationalism[k]    /= np.max((s.race_counts[k],1));
			s.genes[k]          = s.genes[k] / np.max((s.race_counts[k],1));
			s.invasion[k]       /= np.max((s.race_counts[k],1));
		
		
		
		s.alive_races = len(races);
		return s;
					

def plot(g, stats = None, pp = None):
	data = np.array([[(g.board[i][j].race+1) if not g.free(i,j) else 0 for j in range(g.grid_size)] for i in range(g.grid_size)])

	if stats is None:
		stats = g.stats();
	
	# create discrete colormap
	clrs = ['white','red', 'blue', 'green', 'purple'];
	clrs = clrs[:(g.n_races+1)]

	cmap = colors.ListedColormap(clrs)
	bounds = [-0.5,0.5,1.5,2.5,3.5,4.5]
	bounds = bounds[:(g.n_races+2)]
	norm = colors.BoundaryNorm(bounds, cmap.N)

	if pp is None:
		fig, (ax1,ax2) = plt.subplots(1,2,figsize=(15,10))
		img = ax1.imshow(data, cmap=cmap, norm=norm)

		# draw gridlines
		ax1.grid(which='major', axis='both', linestyle='-', color='gray', linewidth=0.5)
		ax1.set_xticks(np.arange(0, g.grid_size, 1));
		ax1.set_yticks(np.arange(0, g.grid_size, 1));
		ax1.set_xticklabels([])
		ax1.set_yticklabels([])

	else:
		fig,(ax1,ax2),img = pp;
		img.set_data(data);
		
		
	ax2.clear();
	ax2.text(0, 0.95, "n agents: "+str(stats.n_agents), color = "black", fontsize=14)
	ax2.text(0.5, 0.95, "Year: "+str(g.time), color = "black", fontsize=20)
	ax2.text(0, 0.90, "n races alive: "+str(stats.alive_races), color = "black", fontsize=14)
	
	for k in range(len(stats.race_counts)):
		ax2.text(0, 0.8-k*0.2, "n: "+str(stats.race_counts[k]), color = clrs[k+1], fontsize=14)
		ax2.text(0, 0.75-k*0.2, "altruism: "+str(round(stats.altruism[k],2)), color = clrs[k+1], fontsize=14)
		ax2.text(0, 0.7-k*0.2, "nationalism: "+str(round(stats.nationalism[k],2)), color = clrs[k+1], fontsize=14)
		ax2.text(0, 0.65-k*0.2, "invasion: "+str(round(stats.invasion[k],4)), color = clrs[k+1], fontsize=14)
		
		ax2.text(0.4, 0.8-k*0.2, "genes: ", color = clrs[k+1], fontsize=14)
		for kk in range(len(stats.race_counts)):
			ax2.text(0.53 + kk*0.09, 0.8-k*0.2, str(round(stats.genes[k][kk],2)), color = clrs[kk+1], fontsize=14)

	fig.canvas.draw()
	plt.show(block=False)
	fig.canvas.flush_events()
		
	return fig,(ax1,ax2),img;
		
def plot_stats(stats):
	n_races = len(stats[0].race_counts)

	race_counts = [];
	nationalism = [];
	altruism = [];
	genes = [[] for i in range(n_races)];
	
	# read data
	for s in stats:
		race_counts.append(s.race_counts);
		nationalism.append(s.nationalism);
		altruism.append(s.altruism);	
		
		for g in range(n_races):
			genes[g].append(s.genes[g])
	
	race_counts = np.array(race_counts)
	altruism = np.array(altruism)
	nationalism = np.array(nationalism)
	
	genes = np.array(genes);
	
	#plot
	clrs = ['white','red', 'blue', 'green', 'purple'];
	fig, axs = plt.subplots(n_races,1,figsize=(15,10))
	for i in range(n_races):				
		axs[i].plot(race_counts[:,i], color = clrs[i+1], linestyle = "-", linewidth = 3)
		ax = axs[i].twinx()
	
		ax.plot(altruism[:,i], color = "gray", linestyle = "-", linewidth = 2)
		# ax.plot(nationalism[:,i], color = "black", linestyle = "-", linewidth = 2)
	
		for k in range(n_races):
			if i == j:
				ls = "-";
				lw = 2;
			else:
				ls = ":"
				lw = 1;
			ax.plot(genes[i,:,k],color = clrs[k+1], linestyle = ls, linewidth = lw)
	
		ax.set_ylim((0,1))
	
		# ax = axs[i].twinx()		
		# ax.plot(race_counts[:,i], color = clrs[i+1], linestyle = "-", linewidth = 3)
	
	fig.canvas.draw()
	plt.show(block=False)
	fig.canvas.flush_events()
	
	fig, ax = plt.subplots(figsize=(15,10))	
	for i in range(n_races):				
		ax.plot(race_counts[:,i], color = clrs[i+1], linestyle = "-", linewidth = 3)
	
	fig.canvas.draw()
	plt.show(block=False)
	fig.canvas.flush_events()
	
	return race_counts, nationalism, altruism, genes
	

		
		
plt.ion()	
grid_size = 50;
n_races = 4;
g = Game(n_races = n_races, grid_size = grid_size);

g4 = int(grid_size/4);
g2 = int(grid_size/2);
for i in range(2):
	for j in range(2):
		race = i*2+j;
		gift = 0.05;
		cost = 0.01;
		# genes = [0.5,0.5,0.5,0.5];
		genes = [0.,0.,0.,0.];
		
		# genes = [np.random.choice([0,1]) for k in range(4)];		
		
		for n in range(5):
			g.board[g4 + i*g2][g4+j*g2]         = Agent(n_races = n_races, race = race, gift = gift, cost = cost, genetics=genes);
			g.board[g4 + i*g2 + 1][g4+j*g2 + 1] = Agent(n_races = n_races, race = race, gift = gift, cost = cost, genetics=genes);
			g.board[g4 + i*g2 - 1][g4+j*g2 - 1] = Agent(n_races = n_races, race = race, gift = gift, cost = cost, genetics=genes);
			g.board[g4 + i*g2 - 1][g4+j*g2 + 1] = Agent(n_races = n_races, race = race, gift = gift, cost = cost, genetics=genes);
			g.board[g4 + i*g2 + 1][g4+j*g2 - 1] = Agent(n_races = n_races, race = race, gift = gift, cost = cost, genetics=genes);


g_start = deepcopy(g)

pp = plot(g);

its = int(input("How many iterations? "))
stats = [];
s = g.stats();
stats.append(s);
for i in tqdm(range(its)):
	g.simulate();
	s = g.stats();
	stats.append(s);
	# pp = plot(g,pp=pp, stats=s);
	
data = plot_stats(stats);
pp = plot(g,pp=pp, stats=s);
	
its = input("waiting to die: ")


























