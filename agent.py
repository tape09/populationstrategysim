

import numpy as np








class Agent:
	def __init__(self,n_races = 2, race = 0, genetics = None, cost = 0.01, gift = 0.05):
		if genetics:
			self.genetics = genetics;
		else:
			self.genetics = [0]*n_races;

		self.race = race;
		
		self.p_reproduction_default = 0.1;
		self.p_reproduction = 0.1;
		self.p_death_default = 0.1;
		self.p_death = 0.1;
		
		self.p_mutate = 0.1;
		self.p_migrate = 0.01;
		
		self.cooperation_cost = cost;
		self.cooperation_gift = gift;
		
		
	def mutate(self):
		### mutate preferences for each race individually
		for i in range(len(self.genetics)):
			if np.random.rand() < self.p_mutate:
				# lst = list(self.genetics);
				# lst[i] = np.random.choice(["0","1"]);
				# self.genetics = "".join(lst);
				# self.genetics[i] = np.random.choice([0,1]);
				self.genetics[i] += np.random.choice([-0.1,0.1]);
				self.genetics[i] = np.min((self.genetics[i],1.0));
				self.genetics[i] = np.max((self.genetics[i],0.0));
				
				
		### mutate only own race vs other races
		# mutate_own = np.random.rand() < self.p_mutate;
		# mutate_other = np.random.rand() < self.p_mutate;
		# for i in range(len(self.genetics)):
			# if mutate_own and i == race:
				# lst = list(self.genetics);
				# lst[i] = np.random.choice(["0","1"]);
				# self.genetics = "".join(lst);
			# if mutate_other and i != race:
				# lst = list(self.genetics);
				# lst[i] = np.random.choice(["0","1"]);
				# self.genetics = "".join(lst);

		## mutate migration
		# if np.random.rand() < self.p_mutate:
			# self.p_migrate += np.random.choice([-0.005,0.005]);
			# self.p_migrate = np.max((0,self.p_migrate));			
			# self.p_migrate = np.min((0.25,self.p_migrate));			
			
				
	def die(self):
		return np.random.rand() < self.p_death;

	def migrate(self):
		return np.random.rand() < self.p_migrate;

	def reproduce(self):
		return np.random.rand() < self.p_reproduction;

	def reset_reproduction(self):
		self.p_reproduction = self.p_reproduction_default;


























